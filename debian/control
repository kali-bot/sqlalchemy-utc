Source: sqlalchemy-utc
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest,
               python3-setuptools,
               python3-sqlalchemy
Standards-Version: 4.5.1
Homepage: https://github.com/spoqa/sqlalchemy-utc
Vcs-Browser: https://gitlab.com/kalilinux/packages/sqlalchemy-utc
Vcs-Git: https://gitlab.com/kalilinux/packages/sqlalchemy-utc.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-sqlalchemy-utc
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: SQLAlchemy type to store aware datetime values (Python 3)
 This package contains a drop-in replacement of SQLAlchemy's built-in DateTime
 type with timezone=True option enabled. Although SQLAlchemy's built-in
 DateTime type provides timezone=True option, since some vendors like SQLite
 and MySQL don't provide timestamptz data type, the option doesn't make any
 effect on these vendors.
 .
 UtcDateTime type is equivalent to the built-in DateTime with timezone=True
 option enabled on vendors that support timestamptz e.g. PostgreSQL, but on
 SQLite or MySQL, it shifts all datetime.datetime values to UTC offset before
 store them, and returns always aware datetime.datetime values through result
 sets.
 .
 This package installs the library for Python 3.
